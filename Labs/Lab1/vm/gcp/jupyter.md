# Jupyter

1. Connect via SSH (or the Google Cloud Console) to your virtual machine.

2. Before we install Jupyter, let's get pip.  Run the following:

Please run the commands one at a time so if an error occurs, you are able to catch it. 

- `sudo apt update`

- `sudo apt upgrade`

- `sudo apt install python3-pip`

If prompted, enter "Y" (for yes) when prompted.  If you're using an
international keyboard, be careful about what mode you're typing in --
we've heard that sometimes a character that looks like a "Y" to a
human isn't recognized by the installer.  If prompted about services (as below), just hit the ENTER key to accept the recommendation:

<img src="img/28.png" width=400>

The `apt` program lets you install software on an Ubuntu system; think
of it like `pip`, for more general (you can install stuff not related
to Python).  Putting `sudo` in front of the command means "do this as
a super user".  You're signed in as a regular user, without permission
to install software by default, so you'll use `sudo` often for
installing tools and other tasks.

You might be prompted with a "Restarting services" menu after running `sudo apt upgrade`.  If so, enter "9". 

<img src="img/31.png" width=700>

3. Now let's use pip3 to install Jupyter (don't use sudo for this one):

```
pip3 install jupyterlab==3.4.5 MarkupSafe==2.0.1
```

4. Run the following commands to install the remaining necessary dependencies for CS320.

```
# Update Environment
echo "Updating environment..."
sudo apt-get -y dist-upgrade
```

```
# Install other apt packages
echo "Installing  zip..."
sudo apt-get -y install zip
```

```
echo "Installing  unzip..."
sudo apt-get -y install unzip
```

```
# Git is an important version control system 
echo "Installing  git..."
sudo apt-get -y install git
```

```
# Nano will be used in this class, but vim or emacs are other two text editors
echo "Installing  nano..."
sudo apt install nano
```

```
# Graphviz is a graph visualization software
echo "Installing  graphviz..."
sudo apt-get -y install graphviz
```

```
# Install general packages
echo "Installing Python packages..."
sudo pip3 install jupyter pandas numpy matplotlib requests statistics graphviz ipython
```

```
# Install for project 3.
echo "...for MP3..."
sudo pip3 install selenium Flask lxml html5lib
```

```
# Install for project 4.
echo "...for MP4..."
sudo pip3 install beautifulsoup4
```

```
# Install for project 5.
echo "...for MP5..."
sudo pip3 install geopandas shapely descartes geopy netaddr
```

```
# Install for project 6.
echo "...for MP6..."
sudo pip3 install rasterio pillow scikit-learn
```

```
# Install Chromium
echo "Installing chromium..."
sudo apt-get install -y chromium-browser
```

```
echo "Installing xdg-utils..."
sudo apt-get install -y xdg-utils 
```

```
# Install Additional Requirements
echo "Installing Chrome requirements..."
sudo apt-get install -y wget libasound2 libgbm1 libnspr4 libnss3 libu2f-udev libvulkan1 && rm -rf /var/lib/apt/lists/*
```

```
sudo wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
```

```
sudo apt install ./google-chrome-stable_current_amd64.deb
```

```
# Install extra Selenium requirements
echo "Installing extra Selenium requirements..."
sudo pip3 install webdriver-manager
```

5. When you start Jupyter notebook remotely, you'll want to set a
password for connecting to it.  Make it a good one, or anybody will be
able to take over your VM! (Whenever you need to enter something, like a password, 
in the terminal, don't worry if nothing is appearing as you're typing. Your keystrokes 
are still registering; the terminal just isn't displaying them!) 
Run the following:

```
python3 -m jupyterlab password
```

**Important!** Choose a strong password.  Anybody on the Internet can
  guess your password an unlimited number of times.  Most semesters at
  least one student loses their VM to malicious actors.

6. Now let's start Jupyter.  Run the following:

```
nohup python3 -m jupyterlab --no-browser --ip=0.0.0.0 --port=2020 &
```

You can now close the terminal window.

7. Now, open up a new browser window, and type `IP:2020` for the URL
(IP should be the External IP of the virtual machine).  You can enter
the same password that you set in step 4:

<img src="img/26.png" width=600>

8. After you login, make sure the setup works (e.g., you can create a
notebook and run code).

Good work on getting Jupyter running on your virtual machine!  We
suggest you bookmark the login page so you can come back to it later.

